import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-submit',
  templateUrl: './submit.component.html',
  styleUrls: ['./submit.component.scss']
})

export class SubmitComponent implements OnInit {
  @Input() buttonStatus:any;

  @Output() sendContentEvent = new EventEmitter<void>()


  show: boolean = false

  constructor() { }

  ngOnInit(): void {
  }
  
  onClick(event: any): void {
    this.show = true
    this.sendContentEvent.emit()
  }

}
