import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})

export class AppService {

  clientsList : any[] = [];

  constructor() {

    let stringClients = localStorage.getItem('clients')

    if(stringClients){
      this.clientsList = JSON.parse(stringClients)
    }
  }
  
  saveDataClients(data: object) {

      this.clientsList.push(data)

      let dataString = JSON.stringify(this.clientsList)
      localStorage.setItem('clients', dataString)
  }
}
