import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {

  @Output() countryContentEvent = new EventEmitter<string>()


  countries:string[] = ['','España', 'Francia', 'Italia']

  selectCorrect = ''
  countryContent = ''

  constructor() { }

  ngOnInit(): void {
  }

  onChange(): void {
    this.selectCorrect = 'selectCorrect'
    this.sendCountryContent()
  }

  sendCountryContent(){
    let data = {
      value: this.countryContent,
      correct: true
    }
    this.countryContentEvent.emit(JSON.stringify(data))
  }
}
