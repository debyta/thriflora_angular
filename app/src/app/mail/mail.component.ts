import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.scss']
})

export class MailComponent implements OnInit {

  @Output() mailContentEvent = new EventEmitter<string>()


  mailExpReg = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
  
  placeholder: string = 'correo@correo.com'
  mailContent: string = ''
  mailConditionCss: string = ''

  constructor() { }

  ngOnInit(): void {
  }
  
  mailValidation(): void {
    if(this.mailExpReg.test(this.mailContent)){
      this.mailConditionCss = 'mailConditionOk'
      this.sendMailContent(true)
    } else{
      this.mailConditionCss = 'mailConditionKO'
      this.sendMailContent(false)
    }   
  }

  keyboard(event: KeyboardEvent): void{
    this.mailValidation()
  }

  onChange(): void {
    this.mailValidation()
  }

  sendMailContent(status:boolean){
    let data = {
      value: this.mailContent,
      correct: status
    }
    this.mailContentEvent.emit(JSON.stringify(data))
  }
}
