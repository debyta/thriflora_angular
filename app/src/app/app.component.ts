import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'Thriflora';
  pinfo = 'Estamos trabajando en nuestra tienda online. Mientras la ponemos a punto, puedes suscribirte a continuación para enterarte la primera de cuándo estará disponible en tu país de residencia'
  credits = 'Thriflora.- Suministrando plantas desde 1988'
  buttonStatus = true
  email = {value:'', correct: false}
  country = {value:'', correct: false}
  checkbox = {value:'', correct: false}

  constructor(private service: AppService) {}

  ngOnInit() {
  }

  getMailContent(event: string): void {
    this.email = JSON.parse(event)
    this.enableButton()
  }

  getCountryContent(event:string): void {
    this.country = JSON.parse(event)
    this.enableButton()
  }

  getCheckboxContent(event:string): void {
    this.checkbox = JSON.parse(event)
    this.enableButton()
  }

  getClickButton(){
    let data = {email: this.email.value,
                country: this.country.value}

    this.service.saveDataClients(data)    
  }

  enableButton(){
    if(this.email.correct && this.country.correct && this.checkbox.correct){
      this.buttonStatus = false
    } else {
      this.buttonStatus = true
    }
  }

}
