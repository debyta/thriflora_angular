import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {

  @Output() checkboxContentEvent = new EventEmitter<string>()


  checkboxContent: boolean = false

  constructor() { }

  ngOnInit(): void {
  }

  onChange(): void {
    this.sendCheckboxContent()
  }

  sendCheckboxContent(){
    let data = {
      value: this.checkboxContent,
      correct: true
    }
    this.checkboxContentEvent.emit(JSON.stringify(data))
  }
}
